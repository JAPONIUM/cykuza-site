import { memo } from "react";
import { banner } from "./Images"

function Banner()
{
    return <>
    <section className="banner-section">
    <div className="banner-text">
	{/* <img src={banner} className="banner-img"/> */}
	<h1>C¥kuza</h1>
	<div className="flex-text">
	    <p>time to unite</p>
	    <p>to hack the C¥</p>
	</div>
    </div>
    </section>
    </>
}
export default memo(Banner);
