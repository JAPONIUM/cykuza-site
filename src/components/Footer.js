import { Col, Container, Row } from "react-bootstrap"
import '../assets/css/footer.css'
import { arrow } from "./Images"
function Footer() {
    return <>
	<footer>
	    <Container fluid>
		<div>

		    <div className="custom-row">
			<div className="custom-col">
			    <h3>C¥KUZA</h3>
			    <ul className="ul">
				<li>
				    <a href="/about">ABOUT</a>
				</li>
				<li>
				    <a href="/gokudo">GOKUDO</a>

				</li>
				<li>
				    <a href="https://satellite.earth/n/C%C2%A5kuza/npub1zxl8m0rwxeqtegv47eclvj3me7x2k00rg3gr26re002rmq49lr5slhll8t" target="_blank" className="active">C¥BOARD</a>
				</li>
			    </ul>
			</div>
			<div className="custom-col">
			    <h3>INFO</h3>
			    <ul className="ul">
				<li>
				    <a href="https://cypedia.cyberyen.org" target="_blank">C¥PEDIA</a>
				</li>
				<li>
				    <a href="https://yakihonne.com/users/nprofile1qqspr0nah3hrvs9u5x2lvu0kfgaulr9t8h35g5p4dpuhh4pas2jl36gpr3mhxue69uhkummnw3ez6vp39eukz6mfdphkumn99e3k7mgpremhxue69uhkummnw3ez6vpj9ejx7unpveskxar0wfujummjvuq3gamnwvaz7tmjv4kxz7fwv3sk6atn9e5k77cdl84" target="_blank">POSTS</a>
				</li>
			    </ul>
			</div>
			<div className="custom-col">
			    <h3>DOCS</h3>
			    <ul className="ul">
				<li>
				    <a href="https://gitlab.com/cykuza/manifesto/-/blob/master/Manifesto.pdf" target="_blank" className="footer-active normal-btn-with-icon"><span>Manifesto</span> <img src={arrow} /></a>

				</li>
			    </ul>
			</div>
		    </div>
			<hr />
		</div>
		<div className="footer-bottom">
		    <div>
			<p className="text-white">&copy; {new Date().getFullYear()} C¥kuza</p>
		    </div>
		    <ul className="social-links">
			<li>
			    <a href="https://snort.social/p/npub1zxl8m0rwxeqtegv47eclvj3me7x2k00rg3gr26re002rmq49lr5slhll8t" target="_blank"><i class="fa-solid fa-hashtag"></i></a>
			</li>
			<li>
			    <a href="https://matrix.to/#/#cykuza:matrix.org" target="_blank"><i class="fa fa-matrix-org fa-lg"></i></a>
			</li>
			<li>
			    <a href="https://gitlab.com/cykuza/" target="_blank"><i class="fa-brands fa-gitlab"></i></a>
			</li>
		    </ul>
		</div>
	    </Container>
	</footer>
    </>
}

export default Footer
