import { memo } from "react"
import { Col, Container, Row } from "react-bootstrap"
import { arrow } from "./Images"

function GokudoSection()
{
    return <>
    <section className="section second-sec bottom-padding">
	    <Container fluid>
	    <h2 id="network-about" className="section-title">GOKUDO</h2>

		<Row>
		    <Col lg={10} md={10}>
			<div className="list-box">
			    <h2 style={{fontWeight: "bold"}}>Engagement:</h2>
			    <div className="intent">
	<a href="https://gitlab.com/cykuza/cypool" target="_blank" className="normal-btn-with-icon"><span>• intent: cypool</span> <img src={arrow}/></a>
			    </div>
			    <div className="intent">
	<a href="https://gitlab.com/cykuza/cypedia" target="_blank" className="normal-btn-with-icon"><span>• intent: cypedia</span> <img src={arrow}/></a>
			    </div>
			    <div className="intent">
				 <a href="https://gitlab.com/cykuza/cydevkit" target="_blank" className="normal-btn-with-icon"><span>• intent: cydevkit</span> <img src={arrow}/></a>
			    </div>
			    <div className="intent">
				 <a href="https://gitlab.com/cykuza/cywallet" target="_blank" className="normal-btn-with-icon"><span>• intent: cywallet</span> <img src={arrow}/></a>
			    </div>
			    <div className="intent">
				 <a href="https://gitlab.com/cykuza/cynode" target="_blank" className="normal-btn-with-icon"><span>• intent: cynode</span> <img src={arrow}/></a>
			    </div>
			    <div className="intent">
				 <a href="https://gitlab.com/cykuza/cyterminal" target="_blank" className="normal-btn-with-icon"><span>• intent: cyterminal</span> <img src={arrow}/></a>
			    </div>
			    <div className="intent">
				 <a href="https://gitlab.com/cykuza/cyradio" target="_blank" className="normal-btn-with-icon"><span>• intent: cyradio</span> <img src={arrow}/></a>
			    </div>
			</div>


		    </Col>
		    <Col lg={10} md={10}>
		    <div className="list-box">
			    <h3 style={{fontWeight: "bold"}}>PREFACE</h3>
			    <p>Cyberyen is a community-driven, consensus based open-source project. Its direction should be determined by the many individual and organisational contributors to the various projects of the Cyberyen blockchain.</p>
			    <p>The development of Cyberyen is driven by interconnecting projects grouped into a single ecosystem, which, according to the community, best define its ideology for the development of a new independent and private blockchain-based payment system.</p>
			    <p>It is very important that Cyberyen remains a decentralized project.</p>
			    <p>The main milestones of Gokudo were focused on creating an open decentralized payment system from Cyberyen to achieve recognition and integration into urban human relations.</p>
			    <p>Emphasis on simplicity, usefulness, reliability and privacy.</p>
			</div>
		    </Col>
		    <Col lg={10} md={10}>

			<div className="list-box">
			    <h3 style={{fontWeight: "bold"}}>TIMELINE</h3>
			    <p>Cyberyen was born in mid-2022. The author who came up with the idea for Cyberyen and published the original version of the code chose to remain anonymous. Without leaving any contact, he gave Cyberyen the opportunity to develop naturally in the open source paradigm.</p>
			    <p>Paradoxically, the first participants of the C¥kuza were united by bugs embedded in the original version of the code, which led to several emergency restarts of the blockchain.
This is a real example of how people can rally around finding solutions to previously unknown problems that have arisen before them.</p>
			    <p>Achieving our goals requires a growing group of people willing to contribute to the development of open source projects.</p>
			    <p>Gokudo from Japanese means “the extreme path”, this word has a deep philosophy, if a person is internally passive and not ready to go to the extreme path, he will never be able to open the other side, which means he will never find the harmony of the middle path, and he will never find true freedom.</p>
			    <p>We need like-minded people who understand the philosophy of our movement and are imbued with the ideas of Cyberyen.</p>
			    <p>Integrators interested in using Cyberyen solutions in their projects also play an important role in development. They will lay a solid foundation and help increase network throughput and scalability in line with real needs.</p>
			    <p>The pace of development will increase as the number and commitment of the community grows. Any help is important, there are no extra people in open source, everyone who contributes, invests in their personal future. Because only by making daily efforts in development and promotion, we become full-fledged and natural users.</p>
			    <p>#C¥kuza</p>
			</div>

		    </Col>
		</Row>
	    </Container>
	</section>
    </>
}

export default memo(GokudoSection)
