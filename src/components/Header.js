import { Link, NavLink } from "react-router-dom"
import { closeicon, logo, showicon } from "./Images"
import '../assets/css/header.css'
import { memo, useContext, useState } from "react"
import GlobalState from "../Context"

function Header() {
    const [connect, setConnect] = useState(false)
    const { isShow, setIsShow } = useContext(GlobalState)
    return <>
	<header>

	    <div className="custom-header">
		<div className="head-flex">
		    <div className="logo-section">
			<Link to="/"><img src={logo} /></Link>
		    </div>
		    <img src={showicon} className="mobile-head-open" onClick={(e) => setIsShow(true)}/>
		</div>
		<div className="header-100-vh">
		<div className={isShow ? "header-body nav-open" : "header-body nav-close"}>
		    <div className="mobile-head">
			<img src={logo} className="sm-logo"/>
			<img src={closeicon} className="close-icon" onClick={(e) => setIsShow(false)}/>
		    </div>
		    <ul className="ul">
			<li className="custom-order-3">
			    <a href="https://gitlab.com/cykuza/manifesto/-/blob/master/Manifesto.pdf" target="_blank" className="active">Manifesto</a>
			</li>
			<li className="custom-order-1">
			    <div className="custom-dropdown">
				<a href="#" >C¥kuza</a>
				<div className="dropdown-body">
				    <ul>
					<li>
					    <a href="/about">About</a>
					</li>
					<li>
					    <a href="/gokudo">Gokudo</a>
					</li>
					<li>
					    <a href="https://satellite.earth/n/C%C2%A5kuza/npub1zxl8m0rwxeqtegv47eclvj3me7x2k00rg3gr26re002rmq49lr5slhll8t" target="_blank">C¥board</a>
					</li>
				    </ul>
				</div>
			    </div>
			</li>
			<li className="custom-order-2">
			<div className="custom-dropdown">
			    <a href="#">Info</a>
			    <div className="dropdown-body">
				    <ul>
					<li>
					    <a href="https://cypedia.cyberyen.org" target="_blank">C¥pedia</a>
					</li>
					<li>
					    <a href="https://yakihonne.com/users/nprofile1qqspr0nah3hrvs9u5x2lvu0kfgaulr9t8h35g5p4dpuhh4pas2jl36gpr3mhxue69uhkummnw3ez6vp39eukz6mfdphkumn99e3k7mgpremhxue69uhkummnw3ez6vpj9ejx7unpveskxar0wfujummjvuq3gamnwvaz7tmjv4kxz7fwv3sk6atn9e5k77cdl84" target="_blank">Posts</a>
					</li>
				    </ul>
				</div>
			    </div>
			</li>
			<div className="custom-order-4 m-d-none">
			    <li >
				<a href="https://gitlab.com/cykuza/" target="_blank"><i class="fa-brands fa-gitlab"></i></a>
			    </li>
			    <li>
				<a href="https://matrix.to/#/#cykuza:matrix.org" target="_blank"><i class="fa fa-matrix-org fa-lg"></i></a>
			    </li>
			</div>
		    </ul>
		    <ul className="ul mobile-social">
			    <li >
				<a href="https://gitlab.com/cykuza/" target="_blank"><i class="fa-brands fa-gitlab"></i></a>
			    </li>
			    <li>
				<a href="https://matrix.to/#/#cykuza:matrix.org" target="_blank"><i class="fa fa-matrix-org fa-lg"></i></a>
			    </li>
		    </ul>
		</div>
		</div>
	    </div>
	</header>
    </>
}

export default memo(Header)
