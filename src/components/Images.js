import logo from '../assets/images/logo.png'
import showicon from '../assets/images/show-icon.png'
import closeicon from '../assets/images/close-icon.png'
import banner from '../assets/images/banner.gif'
import c from '../assets/images/c.png'
import cmobile from '../assets/images/c-mobile.png'
import arrow from '../assets/images/Arrow.png'
export{
  logo,
  showicon,
  closeicon,
  banner,
  c,
  cmobile,
  arrow
}
