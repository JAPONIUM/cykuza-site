import Header from "./Header"
import Banner from "./Banner"
import AboutSection from "./AboutSection"
import GokudoSection from "./GokudoSection"
import Footer from "./Footer"
export{
  Header,
  Banner,
  AboutSection,
  GokudoSection,
  Footer
}
