import { AboutSection, Footer } from "../components"

function About() {
    return <main>
	{/* about section */}
	<AboutSection />
	{/* end about section  */}
  <Footer/>

  </main>
}

export default About
