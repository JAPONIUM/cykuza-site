import { Footer, GokudoSection } from "../components"

function Gokudo() {
    return <main>
	<GokudoSection />
	{/* end second section  */}
	<Footer/>

    </main>
}

export default Gokudo
